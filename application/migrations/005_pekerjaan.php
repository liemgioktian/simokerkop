<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_pekerjaan extends CI_Migration {

  function up () {

    $this->db->query("
      CREATE TABLE `pekerjaan` (
        `uuid` varchar(255) NOT NULL,
        `orders` INT(11) UNIQUE NOT NULL AUTO_INCREMENT,
        `createdAt` datetime DEFAULT NULL,
        `updatedAt` datetime DEFAULT NULL,
        `info_tgl` DATE NOT NULL,
        `info_judul` varchar(255) NOT NULL,
        `info_ket` varchar(255) NOT NULL,
        `info_no_ih` varchar(255) NOT NULL,
        `info_nilai` INT(11) NOT NULL,
        `info_udangan` varchar(255) NOT NULL,
        `info_no_memo` varchar(255) NOT NULL,
        `penawaran_no` varchar(255) NOT NULL,
        `penawaran_tgl` DATE NOT NULL,
        `penawaran_judul` varchar(255) NOT NULL,
        `penawaran_nilai` INT(11) NOT NULL,
        `kontrak_no` varchar(255) NOT NULL,
        `kontrak_tgl` DATE NOT NULL,
        `kontrak_uraian` varchar(255) NOT NULL,
        `kontrak_nilai` INT(11) NOT NULL,
        `kontrak_tgl_terima` DATE NOT NULL,
        `kontrak_tgl_jatuh_tempo` varchar(255) NOT NULL,
        `kontrak_ket` varchar(255) NOT NULL,
        `progress_tgl_penyerahan` DATE NOT NULL,
        `progress_prosentase` INT(11) NOT NULL,
        `progress_ket` varchar(255) NOT NULL,
        `berita_acara_no` varchar(255) NOT NULL,
        `berita_acara_tgl` varchar(255) NOT NULL,
        `berita_acara_ket` varchar(255) NOT NULL,
        `garansi_bank_no` varchar(255) NOT NULL,
        `garansi_bank_tgl` DATE NOT NULL,
        `garansi_bank_tgl_jatuh_tempo` DATE NOT NULL,
        `garansi_bank_nilai` INT(11) NOT NULL,
        `tagihan_no_invoice` varchar(255) NOT NULL,
        `tagihan_tgl_invoice` DATE NOT NULL,
        `tagihan_no_faktur` varchar(255) NOT NULL,
        `tagihan_nilai` INT(11) NOT NULL,
        `tagihan_tgl_penyerahan` varchar(255) NOT NULL,
        `keuangan_tgl_dibayar` DATE NOT NULL,
        `keuangan_nilai_diterima` INT(11) NOT NULL,
        `keuangan_ppn` INT(11) NOT NULL,
        `keuangan_pph` INT(11) NOT NULL,
        `keuangan_denda` INT(11) NOT NULL,
        `keuangan_status` varchar(255) NOT NULL,
        `keuangan_ket` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

  }

  function down () {
    $this->db->query("DROP TABLE IF EXISTS `pekerjaan`");
  }

}
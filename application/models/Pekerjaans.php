<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pekerjaans extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'pekerjaan';
    $this->thead = array(
      (object) array('mData' => 'orders', 'sTitle' => 'No', 'visible' => false),
      (object) array('mData' => 'kontrak_uraian', 'sTitle' => 'Uraian'),
      (object) array('mData' => 'status', 'sTitle' => 'Status', 'width' => '15%'),
      (object) array('mData' => 'sejak', 'sTitle' => 'Sejak', 'width' => '25%')
    );
    $this->form = array (
        array (
		      'name' => 'info_tgl',
		      'label'=> 'Tanggal',
		      'width' => 2,
		      'attributes' => array(
		        array('data-date' => 'datepicker')
			    )),
        array (
				      'name' => 'info_judul',
				      'width' => 2,
		      		'label'=> 'Judul',
					  ),
        array (
				      'name' => 'info_ket',
				      'width' => 2,
		      		'label'=> 'Keterangan',
					  ),
        array (
				      'name' => 'info_no_ih',
				      'width' => 2,
		      		'label'=> 'No. IH',
					  ),
        array (
		      'name' => 'info_nilai',
		      'label'=> 'Nilai',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
				      'name' => 'info_udangan',
				      'width' => 2,
		      		'label'=> 'Undangan / Memo',
					  ),
        array (
				      'name' => 'info_no_memo',
				      'width' => 2,
		      		'label'=> 'No. Memo',
					  ),
        array (
				      'name' => 'penawaran_no',
				      'width' => 2,
		      		'label'=> 'No. Penawaran',
					  ),
        array (
		      'name' => 'penawaran_tgl',
		      'label'=> 'Tanggal Penawaran',
		      'width' => 2,
		      'attributes' => array(
		        array('data-date' => 'datepicker')
			    )),
        array (
				      'name' => 'penawaran_judul',
				      'width' => 2,
		      		'label'=> 'Judul',
					  ),
        array (
				      'name' => 'penawaran_nilai',
				      'width' => 2,
		      		'label'=> 'Nilai',
					  ),
        array (
				      'name' => 'kontrak_no',
				      'width' => 2,
		      		'label'=> 'No',
					  ),
        array (
		      'name' => 'kontrak_tgl',
		      'label'=> 'Tanggal',
		      'width' => 2,
		      'attributes' => array(
		        array('data-date' => 'datepicker')
			    )),
        array (
				      'name' => 'kontrak_uraian',
				      'width' => 2,
		      		'label'=> 'Uraian',
					  ),
        array (
		      'name' => 'kontrak_nilai',
		      'label'=> 'Nilai',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'kontrak_tgl_terima',
		      'label'=> 'Tanggal Terima',
		      'width' => 2,
		      'attributes' => array(
		        array('data-date' => 'datepicker')
			    )),
        array (
				      'name' => 'kontrak_tgl_jatuh_tempo',
				      'width' => 2,
		      		'label'=> 'Tanggal Jatuh Tempo',
					  ),
        array (
				      'name' => 'kontrak_ket',
				      'width' => 2,
		      		'label'=> 'Keterangan',
					  ),
        array (
		      'name' => 'progress_tgl_penyerahan',
		      'label'=> 'Tanggal Penyerahan',
		      'width' => 2,
		      'attributes' => array(
		        array('data-date' => 'datepicker')
			    )),
        array (
		      'name' => 'progress_prosentase',
		      'label'=> 'Prosentase Progress',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
				      'name' => 'progress_ket',
				      'width' => 2,
		      		'label'=> 'Keterangan',
					  ),
        array (
				      'name' => 'berita_acara_no',
				      'width' => 2,
		      		'label'=> 'No. Berita Acara',
					  ),
        array (
				      'name' => 'berita_acara_tgl',
				      'width' => 2,
		      		'label'=> 'Tanggal',
					  ),
        array (
				      'name' => 'berita_acara_ket',
				      'width' => 2,
		      		'label'=> 'Keterangan',
					  ),
        array (
				      'name' => 'garansi_bank_no',
				      'width' => 2,
		      		'label'=> 'No',
					  ),
        array (
		      'name' => 'garansi_bank_tgl',
		      'label'=> 'Tanggal',
		      'width' => 2,
		      'attributes' => array(
		        array('data-date' => 'datepicker')
			    )),
        array (
		      'name' => 'garansi_bank_tgl_jatuh_tempo',
		      'label'=> 'Tanggal Jatuh Tempo',
		      'width' => 2,
		      'attributes' => array(
		        array('data-date' => 'datepicker')
			    )),
        array (
		      'name' => 'garansi_bank_nilai',
		      'label'=> 'Nilai',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
				      'name' => 'tagihan_no_invoice',
				      'width' => 2,
		      		'label'=> 'No. Invoice',
					  ),
        array (
		      'name' => 'tagihan_tgl_invoice',
		      'label'=> 'Tanggal Invoice',
		      'width' => 2,
		      'attributes' => array(
		        array('data-date' => 'datepicker')
			    )),
        array (
				      'name' => 'tagihan_no_faktur',
				      'width' => 2,
		      		'label'=> 'No. Faktur',
					  ),
        array (
		      'name' => 'tagihan_nilai',
		      'label'=> 'Nilai Tagihan',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
				      'name' => 'tagihan_tgl_penyerahan',
				      'width' => 2,
		      		'label'=> 'Tanggal Penyerahan Tagihan',
					  ),
        array (
		      'name' => 'keuangan_tgl_dibayar',
		      'label'=> 'Tanggal Dibayar',
		      'width' => 2,
		      'attributes' => array(
		        array('data-date' => 'datepicker')
			    )),
        array (
		      'name' => 'keuangan_nilai_diterima',
		      'label'=> 'Nilai Diterima',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'keuangan_ppn',
		      'label'=> 'PPN',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'keuangan_pph',
		      'label'=> 'PPh',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
		      'name' => 'keuangan_denda',
		      'label'=> 'Denda',
		      'width' => 2,
		      'attributes' => array(
		        array('data-number' => 'true')
			    )),
        array (
				      'name' => 'keuangan_status',
				      'width' => 2,
		      		'label'=> 'Status',
					  ),
        array (
				      'name' => 'keuangan_ket',
				      'width' => 2,
		      		'label'=> 'Keterangan',
					  ),
    );
    $this->childs = array (
    );
  }

  function dt () {
    $this->datatables
      ->select("{$this->table}.uuid")
      ->select("{$this->table}.orders")
      ->select('pekerjaan.kontrak_uraian')
      ->select("
      	CASE
      		WHEN (keuangan_tgl_dibayar IS NOT NULL AND keuangan_tgl_dibayar NOT IN ('0000-00-00', '', '1970-01-01')) THEN 'DIBAYAR'
      		WHEN (tagihan_tgl_invoice IS NOT NULL AND tagihan_tgl_invoice NOT IN ('0000-00-00', '', '1970-01-01')) THEN 'INVOICE'
      		WHEN (garansi_bank_tgl IS NOT NULL AND garansi_bank_tgl NOT IN ('0000-00-00', '', '1970-01-01')) THEN 'GARANSI BANK'
      		WHEN (berita_acara_tgl IS NOT NULL AND berita_acara_tgl NOT IN ('0000-00-00', '', '1970-01-01')) THEN 'BERITA ACARA'
      		WHEN (progress_tgl_penyerahan IS NOT NULL AND progress_tgl_penyerahan NOT IN ('0000-00-00', '', '1970-01-01')) THEN 'DALAM PENGERJAAN'
      		WHEN (kontrak_tgl IS NOT NULL AND kontrak_tgl NOT IN ('0000-00-00', '', '1970-01-01')) THEN 'KONTRAK'
      		WHEN (penawaran_tgl IS NOT NULL AND penawaran_tgl NOT IN ('0000-00-00', '', '1970-01-01')) THEN 'PENAWARAN'
      		WHEN (info_tgl IS NOT NULL AND info_tgl NOT IN ('0000-00-00', '', '1970-01-01')) THEN 'INFORMASI'
      	ELSE 'TIDAK DIKETAHUI'
      	END status
      ", false)

      ->select("
      	CASE
      		WHEN (keuangan_tgl_dibayar IS NOT NULL AND keuangan_tgl_dibayar NOT IN ('0000-00-00', '', '1970-01-01')) THEN

		      	CASE
		      		WHEN TIMESTAMPDIFF(YEAR, keuangan_tgl_dibayar, NOW()) > 0 THEN CONCAT(DATE_FORMAT(keuangan_tgl_dibayar, '%e %b %Y'), ' ( ',  TIMESTAMPDIFF(YEAR, keuangan_tgl_dibayar, NOW()), ' tahun yang lalu )')
		      		WHEN TIMESTAMPDIFF(MONTH, keuangan_tgl_dibayar, NOW()) > 0 THEN CONCAT(DATE_FORMAT(keuangan_tgl_dibayar, '%e %b %Y'), ' ( ', TIMESTAMPDIFF(MONTH, keuangan_tgl_dibayar, NOW() ) % 12, ' bulan yang lalu )')
		      		WHEN TIMESTAMPDIFF(DAY, keuangan_tgl_dibayar, NOW()) > 0 THEN CONCAT(DATE_FORMAT(keuangan_tgl_dibayar, '%e %b %Y'), ' ( ', FLOOR(TIMESTAMPDIFF(DAY, keuangan_tgl_dibayar, NOW() ) % 30.4375 ), ' hari yang lalu )')
		      	ELSE DATE_FORMAT(keuangan_tgl_dibayar, '%e %b %Y')
		      	END

      		WHEN (tagihan_tgl_invoice IS NOT NULL AND tagihan_tgl_invoice NOT IN ('0000-00-00', '', '1970-01-01')) THEN

		      	CASE
		      		WHEN TIMESTAMPDIFF(YEAR, tagihan_tgl_invoice, NOW()) > 0 THEN CONCAT(DATE_FORMAT(tagihan_tgl_invoice, '%e %b %Y'), ' ( ',  TIMESTAMPDIFF(YEAR, tagihan_tgl_invoice, NOW()), ' tahun yang lalu )')
		      		WHEN TIMESTAMPDIFF(MONTH, tagihan_tgl_invoice, NOW()) > 0 THEN CONCAT(DATE_FORMAT(tagihan_tgl_invoice, '%e %b %Y'), ' ( ', TIMESTAMPDIFF(MONTH, tagihan_tgl_invoice, NOW() ) % 12, ' bulan yang lalu )')
		      		WHEN TIMESTAMPDIFF(DAY, tagihan_tgl_invoice, NOW()) > 0 THEN CONCAT(DATE_FORMAT(tagihan_tgl_invoice, '%e %b %Y'), ' ( ', FLOOR(TIMESTAMPDIFF(DAY, tagihan_tgl_invoice, NOW() ) % 30.4375 ), ' hari yang lalu )')
		      	ELSE DATE_FORMAT(tagihan_tgl_invoice, '%e %b %Y')
		      	END

      		WHEN (garansi_bank_tgl IS NOT NULL AND garansi_bank_tgl NOT IN ('0000-00-00', '', '1970-01-01')) THEN

		      	CASE
		      		WHEN TIMESTAMPDIFF(YEAR, garansi_bank_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(garansi_bank_tgl, '%e %b %Y'), ' ( ',  TIMESTAMPDIFF(YEAR, garansi_bank_tgl, NOW()), ' tahun yang lalu )')
		      		WHEN TIMESTAMPDIFF(MONTH, garansi_bank_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(garansi_bank_tgl, '%e %b %Y'), ' ( ', TIMESTAMPDIFF(MONTH, garansi_bank_tgl, NOW() ) % 12, ' bulan yang lalu )')
		      		WHEN TIMESTAMPDIFF(DAY, garansi_bank_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(garansi_bank_tgl, '%e %b %Y'), ' ( ', FLOOR(TIMESTAMPDIFF(DAY, garansi_bank_tgl, NOW() ) % 30.4375 ), ' hari yang lalu )')
		      	ELSE DATE_FORMAT(garansi_bank_tgl, '%e %b %Y')
		      	END

      		WHEN (berita_acara_tgl IS NOT NULL AND berita_acara_tgl NOT IN ('0000-00-00', '', '1970-01-01')) THEN

		      	CASE
		      		WHEN TIMESTAMPDIFF(YEAR, berita_acara_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(berita_acara_tgl, '%e %b %Y'), ' ( ',  TIMESTAMPDIFF(YEAR, berita_acara_tgl, NOW()), ' tahun yang lalu )')
		      		WHEN TIMESTAMPDIFF(MONTH, berita_acara_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(berita_acara_tgl, '%e %b %Y'), ' ( ', TIMESTAMPDIFF(MONTH, berita_acara_tgl, NOW() ) % 12, ' bulan yang lalu )')
		      		WHEN TIMESTAMPDIFF(DAY, berita_acara_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(berita_acara_tgl, '%e %b %Y'), ' ( ', FLOOR(TIMESTAMPDIFF(DAY, berita_acara_tgl, NOW() ) % 30.4375 ), ' hari yang lalu )')
		      	ELSE DATE_FORMAT(berita_acara_tgl, '%e %b %Y')
		      	END

      		WHEN (progress_tgl_penyerahan IS NOT NULL AND progress_tgl_penyerahan NOT IN ('0000-00-00', '', '1970-01-01')) THEN

		      	CASE
		      		WHEN TIMESTAMPDIFF(YEAR, progress_tgl_penyerahan, NOW()) > 0 THEN CONCAT(DATE_FORMAT(progress_tgl_penyerahan, '%e %b %Y'), ' ( ',  TIMESTAMPDIFF(YEAR, progress_tgl_penyerahan, NOW()), ' tahun yang lalu )')
		      		WHEN TIMESTAMPDIFF(MONTH, progress_tgl_penyerahan, NOW()) > 0 THEN CONCAT(DATE_FORMAT(progress_tgl_penyerahan, '%e %b %Y'), ' ( ', TIMESTAMPDIFF(MONTH, progress_tgl_penyerahan, NOW() ) % 12, ' bulan yang lalu )')
		      		WHEN TIMESTAMPDIFF(DAY, progress_tgl_penyerahan, NOW()) > 0 THEN CONCAT(DATE_FORMAT(progress_tgl_penyerahan, '%e %b %Y'), ' ( ', FLOOR(TIMESTAMPDIFF(DAY, progress_tgl_penyerahan, NOW() ) % 30.4375 ), ' hari yang lalu )')
		      	ELSE DATE_FORMAT(progress_tgl_penyerahan, '%e %b %Y')
		      	END

      		WHEN (kontrak_tgl IS NOT NULL AND kontrak_tgl NOT IN ('0000-00-00', '', '1970-01-01')) THEN

		      	CASE
		      		WHEN TIMESTAMPDIFF(YEAR, kontrak_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(kontrak_tgl, '%e %b %Y'), ' ( ',  TIMESTAMPDIFF(YEAR, kontrak_tgl, NOW()), ' tahun yang lalu )')
		      		WHEN TIMESTAMPDIFF(MONTH, kontrak_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(kontrak_tgl, '%e %b %Y'), ' ( ', TIMESTAMPDIFF(MONTH, kontrak_tgl, NOW() ) % 12, ' bulan yang lalu )')
		      		WHEN TIMESTAMPDIFF(DAY, kontrak_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(kontrak_tgl, '%e %b %Y'), ' ( ', FLOOR(TIMESTAMPDIFF(DAY, kontrak_tgl, NOW() ) % 30.4375 ), ' hari yang lalu )')
		      	ELSE DATE_FORMAT(kontrak_tgl, '%e %b %Y')
		      	END

      		WHEN (penawaran_tgl IS NOT NULL AND penawaran_tgl NOT IN ('0000-00-00', '', '1970-01-01')) THEN

		      	CASE
		      		WHEN TIMESTAMPDIFF(YEAR, penawaran_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(penawaran_tgl, '%e %b %Y'), ' ( ',  TIMESTAMPDIFF(YEAR, penawaran_tgl, NOW()), ' tahun yang lalu )')
		      		WHEN TIMESTAMPDIFF(MONTH, penawaran_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(penawaran_tgl, '%e %b %Y'), ' ( ', TIMESTAMPDIFF(MONTH, penawaran_tgl, NOW() ) % 12, ' bulan yang lalu )')
		      		WHEN TIMESTAMPDIFF(DAY, penawaran_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(penawaran_tgl, '%e %b %Y'), ' ( ', FLOOR(TIMESTAMPDIFF(DAY, penawaran_tgl, NOW() ) % 30.4375 ), ' hari yang lalu )')
		      	ELSE DATE_FORMAT(penawaran_tgl, '%e %b %Y')
		      	END

      		WHEN (info_tgl IS NOT NULL AND info_tgl NOT IN ('0000-00-00', '', '1970-01-01')) THEN

		      	CASE
		      		WHEN TIMESTAMPDIFF(YEAR, info_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(info_tgl, '%e %b %Y'), ' ( ',  TIMESTAMPDIFF(YEAR, info_tgl, NOW()), ' tahun yang lalu )')
		      		WHEN TIMESTAMPDIFF(MONTH, info_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(info_tgl, '%e %b %Y'), ' ( ', TIMESTAMPDIFF(MONTH, info_tgl, NOW() ) % 12, ' bulan yang lalu )')
		      		WHEN TIMESTAMPDIFF(DAY, info_tgl, NOW()) > 0 THEN CONCAT(DATE_FORMAT(info_tgl, '%e %b %Y'), ' ( ', FLOOR(TIMESTAMPDIFF(DAY, info_tgl, NOW() ) % 30.4375 ), ' hari yang lalu )')
		      	ELSE DATE_FORMAT(info_tgl, '%e %b %Y')
		      	END

      	ELSE 'TIDAK DIKETAHUI'
      	END sejak
      ", false);
    return parent::dt();
  }

}